#pragma once

#include "Mesh.hpp"

#include <iostream>
#include <vector>

glm::vec3 getVertices(float C, float theta, float phi) {
    return glm::vec3(sin(2 * theta) * pow(cos(phi), 2) * C, sin(theta) * sin(2 * phi) * C, cos(theta) * sin(2 * phi) * C);
}

glm::vec3 getNormals(float C, float theta, float phi) {
    return glm::vec3(sin(theta) * pow(cos(phi), 2) * (3 * cos(2 * phi) - 1) - 2 * sin(3 * theta) * pow(cos(phi), 4),
        4 * cos(theta) * pow(cos(phi), 2) * (pow(sin(theta), 2) - pow(cos(theta), 2) * cos(2 * phi)),
        -sin(4 * phi));
}


MeshPtr makeSurface(float C = 1.0f, unsigned int N = 100, float shift = 0.0f)
{
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texCoords;
    
    for (unsigned int i = 0; i < N; ++i)
    {
        float theta = (float)glm::pi<float>() * i / N;
        float theta1 = (float)glm::pi<float>() * (i + 1) / N;
        
        for (unsigned int j = 0; j < N; ++j)
        {
	    	float phi = (float)glm::pi<float>() * j / N;
            float phi1 = (float)glm::pi<float>() * (j + 1) / N;

            vertices.push_back(getVertices(C, theta, phi));
            vertices.push_back(getVertices(C, theta1, phi));
            vertices.push_back(getVertices(C, theta1, phi1));

            normals.push_back(getNormals(C, theta, phi));
            normals.push_back(getNormals(C, theta1, phi));
            normals.push_back(getNormals(C, theta1, phi1));

            vertices.push_back(getVertices(C, theta, phi));
            vertices.push_back(getVertices(C, theta, phi1));
            vertices.push_back(getVertices(C, theta1, phi1));

            normals.push_back(getNormals(C, theta, phi));
            normals.push_back(getNormals(C, theta, phi1));
            normals.push_back(getNormals(C, theta1, phi1));
        }
    }

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());
    
    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());
    
    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount((GLint)vertices.size());
    
    return mesh;
}
