#include "common/Application.hpp"
#include "common/Mesh.hpp"
#include "common/ShaderProgram.hpp"
#include "common/RomanSurface.hpp"

#include <iostream>

class CrossCapSurfaceApplication : public Application
{
public:
    ShaderProgramPtr _shader;
    
    void makeScene() override
    {
        Application::makeScene();
        
        _crossCapSurface = makeSurface(1.0f, (int)(_detalizationFactor / 30));
        _crossCapSurface->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.5f)));

        _cameraMover = std::make_shared<OrbitCameraMover>();
        _shader = std::make_shared<ShaderProgram>(
                "./691IvanovData1/shaderNormal.vert",
                "./691IvanovData1/shader.frag"
        );
    }
	
    void update() override
    {
        double dt = glfwGetTime() - _oldTime;
        _oldTime = glfwGetTime();
		
        _cameraMover->update(_window, dt);
        _camera = _cameraMover->cameraInfo();
	
        if (glfwGetKey(_window, GLFW_KEY_EQUAL) == GLFW_PRESS)
        {
            if (_detalizationFactor + 1 <= 3000)
            {
                ++_detalizationFactor;
            }
        }
        if (glfwGetKey(_window, GLFW_KEY_MINUS) == GLFW_PRESS)
        {
            if (_detalizationFactor - 1 >= 90)
            {
                --_detalizationFactor;
            }
        }
        std::cout << "Detalization: " << (int)(_detalizationFactor / 30) << std::endl;

        _crossCapSurface = makeSurface(1.0f, (int)(_detalizationFactor / 30));
        _crossCapSurface->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.5f)));
    }
    
    void draw() override
    {
        Application::draw();

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);
        
        glViewport(0, 0, width, height);
        
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        _shader->use();
        
        _shader->setFloatUniform("time", (float)glfwGetTime());
        
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
        
        _shader->setMat4Uniform("modelMatrix", _crossCapSurface->modelMatrix());
        _crossCapSurface->draw();
    }
	
private:
    MeshPtr _crossCapSurface;
    unsigned int _detalizationFactor = 90;
};

int main()
{
    CrossCapSurfaceApplication app;
    app.start();
}