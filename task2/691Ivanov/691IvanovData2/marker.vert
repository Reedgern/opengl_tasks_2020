#version 330

uniform mat4 mvpMatrix; 

layout(location = 0) in vec3 vertexPosition;

void main()
{	
	gl_Position = mvpMatrix * vec4(vertexPosition, 1.0);
}